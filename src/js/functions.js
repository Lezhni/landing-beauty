function showPopup(popupID) {

	if (!popupID) { return false; }

	$.magnificPopup.open({
		items: { src: popupID },
	  	type: 'inline',
	  	fixedContentPos: true,
	  	callbacks: {
	  		beforeOpen: function() {
	  			scrollPos = $(window).scrollTop();
	  			$('html').addClass('mfp-helper');
	  		},
    		close: function() { 
    			$('html').removeClass('mfp-helper');
    			$(window).scrollTop(scrollPos);
    		}
	  	}
	});
}