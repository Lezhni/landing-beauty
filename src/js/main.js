var scrollPos;

$(document).on('ready', function() {

	// init sliders
    if ($(window).width() > 1023) {
    	$('.js-clients-slider').glide({
        	type: 'carousel'
    	});
    }

	// phone input mask
	$('input[type="tel"]').mask('+7 (999) 999-99-99');
});

// open popup on click `js-popup` class
$('.js-popup').on('click', function(e) {
	e.preventDefault();

	var target = $(this).attr('href');
	showPopup(target);
});

// open video
$('.js-video').magnificPopup({
	type: 'iframe',
	mainClass: 'mfp-fade',
	removalDelay: 160,
	preloader: false,
	fixedContentPos: false
});

// open mobile menu
$('.js-menu').on('click', function() {

	var panel = $('.mobile');
	$('body').toggleClass('menu-opened');

	if (panel.attr('aria-hidden') == 'true') {
		panel.attr('aria-hidden', 'false');
	} else {
		panel.attr('aria-hidden', 'true');
	}
});

// scroll to blocks
$('.js-scroll').on('click', function(e) {
	e.preventDefault();

	var target = $(this).attr('href');

	if (typeof target !== 'undefined') {
		$('html, body').animate({
			scrollTop: ($(target).offset().top - 70) + 'px' // 70 - header height
		}, 1000);
	}
});

$('form').on('submit', function(e) {
	e.preventDefault();

    var form = $(this);
    var formData = form.serialize();

    form.find('[type=submit]').attr('disabled', 'disabled');

    if (formData) {
        $.post('send.php', formData, function(data) {
            if (data == 'sended') {
            	yaCounter41764544.reachGoal('online_zakaz');
            	console.log('Yandex.Metrika goal reached');
                showPopup('#thanks');
            } else {
               	console.log(data);
            }
        });
    }

    form.find('[type=submit]').removeAttr('disabled');
});

document.onreadystatechange = function() {
	if (document.readyState == "complete") {

		//init map in contacts
	    ymaps.ready(function() {

	        var address = {
	        	title: 'г. Санкт-Петербург, Коломяжский пр-т, д. 26',
	        	coords: [60.004244, 30.295135]
	        };

	        map = new ymaps.Map("contacts-map", {
		        center: address.coords,
		        zoom: 15,
		    }, { suppressMapOpenBlock: true });

		    map.controls.add('zoomControl');

		    if ($(window).innerWidth() < 1024) {
		    	map.behaviors.disable('drag');
		    }

		    placemark = new ymaps.Placemark(address.coords, {
		        hintContent: address.title
		    }, {
		        iconLayout: "default#image",
		        iconImageHref: 'img//map.png',
		        iconImageSize: [52, 64],
		        iconImageOffset: [-26, -64]
		    });

		    map.geoObjects.add(placemark);
	    });
	}
};