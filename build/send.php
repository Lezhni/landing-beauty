<?php
if ($_POST && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    require 'phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';
    $mail->setFrom('inforsalon@yandex.ru', 'Wella Podium Salon');
    $mail->addReplyTo('inforsalon@yandex.ru', 'Wella Podium Salon');
    $mail->addAddress('inforsalon@yandex.ru');
    $mail->isHTML(true);

    $mail->Subject = 'Wella Podium Salon: заявка с сайта';
    $mail->Body = "";
    
    if ($_POST['contact']) $mail->Body .= "<b>Имя:</b> {$_POST['contact']}<br><br>";
    if ($_POST['phone']) $mail->Body .= "<b>Номер телефона:</b> {$_POST['phone']}<br><br>";

    if($mail->send()) {
        echo 'sended';
    } else {
        echo $mail->ErrorInfo;
    }
}

die();